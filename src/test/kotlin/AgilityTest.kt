import org.hamcrest.CoreMatchers.`is`
import org.junit.Assert.*
import org.junit.Test
import kotlin.test.*

//@RunWith(JUnit4::class)
class AgilityTest {
    private val agile = Agility()

    @Test
    fun whenBiggerThan(){
        assertTrue(agile.biggerThan("100","10"))
        assertTrue(agile.biggerThan("0.05","0.005"))
        assertFalse(agile.biggerThan("10","100"))
        assertFalse(agile.biggerThan("0.01","0.2"))
    }

    @Test
    fun whenOrder() {

        assertContentEquals(listOf(2,4,8,10,15), agile.order(10, 15, 2, 4, 8))
        assertContentEquals(listOf(-10,2,4,8,15), agile.order(-10, 15, 2, 4, 8))
        assertContentEquals(listOf(-7,-4,0,10,15), agile.order(0, 15, -7, -4, 10))

    }

    @Test
    fun smallerThan() {
        assertEquals(-1.0, agile.smallerThan(listOf(0.0, 10.1, -1.0, -0.2, 3.2, 200.1)), 0.0)
        assertEquals(10.0, agile.smallerThan(listOf(10.0, 10.1, 20.0, 30.2)), 0.0)
        assertEquals(-100.1, agile.smallerThan(listOf(3.0, 45.12, 78.0, 0.0, 10.1, -1.0, -0.2, 3.2, -100.1)), 0.0)
        assertEquals(-12.0, agile.smallerThan(listOf(0.0, 10.1, -1.0, -0.2, 3.2, 200.1, -12.0)), 0.0)

    }

    @Test
    fun palindromeNumber() {
        assertTrue(agile.palindromeNumber(123321))
        assertTrue(agile.palindromeNumber(1234321))
        assertTrue(agile.palindromeNumber(1237321))
        assertFalse(agile.palindromeNumber(123))
        assertFalse(agile.palindromeNumber(345))

    }

    @Test
    fun palindromeWord() {
        assertTrue(agile.palindromeWord("civic"))
        assertTrue(agile.palindromeWord("solos"))
        assertTrue(agile.palindromeWord("rotator"))
        assertFalse(agile.palindromeWord("sigmotoa"))
    }

    @Test
    fun factorial() {
        assertEquals(1,agile.factorial(0))
        assertEquals(1,agile.factorial(1))
        assertEquals(6, agile.factorial(3))
        assertEquals(3628800, agile.factorial(10))

    }

    @Test
    fun is_Odd() {
        assertTrue(agile.is_Odd(numA = -7))
        assertFalse(agile.is_Odd(-2))
        assertTrue(agile.is_Odd(numA = 80))
        assertTrue(agile.is_Odd(numA = 125))
        assertFalse(agile.is_Odd(numA = -2))
    }

    @Test
    fun isPrimeNumber() {
        assertFalse(agile.isPrimeNumber(6))
        assertFalse(agile.isPrimeNumber(numA = 10))
        assertFalse(agile.isPrimeNumber(numA = -1))
        assertTrue(agile.isPrimeNumber(numA = 2))
        assertTrue(agile.isPrimeNumber(7))
        assertTrue(agile.isPrimeNumber(97))
    }

    @Test
    fun is_Even() {

        assertTrue(agile.is_Even(numA = -8))
        assertTrue(agile.is_Even(numA = 124))
        assertTrue(agile.is_Even(numA = 14))
        assertFalse(agile.is_Even(numA = -7))
        assertFalse(agile.is_Even(numA = 81))

    }

    @Test
    fun isPerfectNumber() {

        assertTrue(agile.isPerfectNumber(numA = 6))
        assertTrue(agile.isPerfectNumber(numA = 496))
        assertFalse(agile.isPerfectNumber(100))
        assertFalse(agile.isPerfectNumber(57))
    }

    @Test
    fun fibonacci() {
        assertContentEquals(listOf(0,1,1,2,3,5,8,13,21,34,55), agile.fibonacci(10))
        assertContentEquals(listOf(0,1,1,2,3,5,8,13,21), agile.fibonacci(8))
        assertContentEquals(listOf(0,1,1), agile.fibonacci(2))
        assertContentEquals(listOf(0,1,1,2,3,5,8,13,21,34,55,89), agile.fibonacci(11))
    }

    @Test
    fun timesDividedByThree() {
        assertEquals(1, agile.timesDividedByThree(3))
        assertEquals(1, agile.timesDividedByThree(4))
        assertEquals(1, agile.timesDividedByThree(5))
        assertEquals(2, agile.timesDividedByThree(6))
        assertEquals(2, agile.timesDividedByThree(7))
        assertNotEquals(3, agile.timesDividedByThree(3))
    }

    @Test
    fun fizzBuzz() {


        assertThat(agile.fizzBuzz(6), `is`("Fizz"))
        assertThat(agile.fizzBuzz(3), `is`("Fizz"))
        assertThat(agile.fizzBuzz(5), `is`("Buzz"))
        assertThat(agile.fizzBuzz(10), `is`("Buzz"))
        assertThat(agile.fizzBuzz(15), `is`("FizzBuzz"))
        assertThat(agile.fizzBuzz(30), `is`("FizzBuzz"))
        assertThat(agile.fizzBuzz(2), `is`("2"))
        assertThat(agile.fizzBuzz(16), `is`("16"))
        assertThat(agile.fizzBuzz(4), `is`("4"))



    }
}